//
//  ChecklistItem.swift
//  Party Planner Pro
//
//  Created by Janie on 10/23/16.
//  Copyright © 2016 software2. All rights reserved.
//

import Foundation

class ChecklistItem {
    var name: String? = ""
    var checked: Bool = false
}
