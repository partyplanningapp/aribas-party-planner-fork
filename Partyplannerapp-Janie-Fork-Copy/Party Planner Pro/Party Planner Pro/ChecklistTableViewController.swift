//
//  ChecklistTableViewController.swift
//  Party Planner Pro
//
//  Created by Janie on 10/23/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class ChecklistTableViewController: UITableViewController {
    
    var items: [ChecklistItem] = []
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let newItem = ChecklistItem()
        newItem.name = "Item Name"
        newItem.checked = false
        items.append(newItem)
    }
    
    @IBAction func addItem(sender: AnyObject?) {
        let alertController = UIAlertController(title: "Add item", message: "Enter name of item", preferredStyle: .alert)
        
        alertController.addTextField { (namefield:UITextField) in
            
        }
        
        let addAction = UIAlertAction(title: "Add", style: .default) { (action:UIAlertAction) in
            let itemname = alertController.textFields?.first
            
            let item = ChecklistItem()
            item.name = itemname!.text!
            self.items.append(item)
            
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action: UIAlertAction) in
        }
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.purple
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return items.count
    }

    func configureCheckmarkForCell(cell: UITableViewCell, index: Int) {
        let isChecked = items[index].checked
        if (isChecked) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
    }
    
    //Manages text in each individual cell
    func configureTextForCell(cell: UITableViewCell, item: ChecklistItem) {
         let label: UILabel = cell.viewWithTag(100) as! UILabel
         label.text = item.name
        label.textColor = UIColor.white
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Item", for: indexPath)
        let item: ChecklistItem = items[indexPath.row]
        
        configureTextForCell(cell: cell, item: item)
        configureCheckmarkForCell(cell: cell, index: indexPath.row)
        
        cell.backgroundColor = UIColor.clear
       
        
        return cell
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        let item: ChecklistItem = items[indexPath.row]
        item.checked = !item.checked
        
        configureCheckmarkForCell(cell: cell, index: indexPath.row)
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            items.remove(at: indexPath.row)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
