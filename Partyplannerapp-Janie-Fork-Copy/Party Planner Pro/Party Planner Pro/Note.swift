//
//  Note.swift
//  party planner pro
//
//  Created by Ariba Siddiqui on 9/30/16.
//  Copyright © 2016 software2. All rights reserved.
//

import Foundation

class Note {
    var title = ""
    var content = ""
}
