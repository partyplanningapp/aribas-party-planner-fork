//
//  NotesListTableTableViewController.swift
//  party planner pro
//
//  Created by Ariba Siddiqui on 9/30/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class NotesListTableTableViewController: UITableViewController {
    
    var notes: [Note] = []
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let note1 = Note()
        note1.title = "Guests"
        note1.content = "John Doe"
        
        let note2 = Note()
        note2.title = "Menu"
        note2.content = "Type Menu Here"
        notes.append(note1)
        notes.append(note2)
    }
    
    
    @IBAction func addNoteName(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Holiday Party Notes", message: "Add Note", preferredStyle: .alert)
        
        alertController.addTextField { (textfield:UITextField) in
            
        }
        
        let addAction = UIAlertAction(title: "Add", style: .default) { (action:UIAlertAction) in
            let textfield = alertController.textFields?.first
            
            let noteAdded = Note()
            noteAdded.title = textfield!.text!
            
            self.notes.append(noteAdded)
            
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action: UIAlertAction) in
        }
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.purple
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    /*
     override func numberOfSections(in tableView: UITableView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }
     */
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return notes.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath)
        
        // Configure the cell...
        
        cell.textLabel!.text = notes[indexPath.row].title
        cell.backgroundColor = UIColor.clear
        cell.textLabel!.textColor = UIColor.white
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier! == "showNote" {
            /*let checklistViewController = segue.destination as! ChecklistTableViewController
            var selectedIndexPath = tableView.indexPathForSelectedRow*/
            //noteDetailViewController.note = notes[selectedIndexPath!.row]
            
        }
        else if segue.identifier! == "addNote" {
            let note = Note()
            notes.append(note)
            let noteDetailViewController = segue.destination as! NoteDetailViewController
            noteDetailViewController.note = note
        }
        else if segue.identifier! == "guestSegue"{
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            
            /*
             let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
             let destination = storyboard.instantiateViewController(withIdentifier: "GuestViewController") as! GuestViewController
             navigationController?.pushViewController(destination, animated: true)
             */
            
            self.performSegue(withIdentifier: "guestSegue", sender: self)
        }
        else
        {
            self.performSegue(withIdentifier: "showNote", sender: self)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            notes.remove(at: indexPath.row)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
