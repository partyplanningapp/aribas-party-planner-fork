//
//  ViewController.swift
//  partyplannerprotest
//
//  Created by Baron Heinrich on 10/13/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var name_txt: UITextField!
    var name = ""
    var themeID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 122/255, green: 0/255, blue: 89/255, alpha: 1.0)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
   
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        //need to insert themeID into the database here
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            themeID = 0
            print(themeID)
        case 1:
            themeID = 1
            print(themeID)
        case 2:
            themeID = 2
            print(themeID)
        case 3:
            themeID = 3
            print(themeID)
        default:
            break; 
        }
    }
    @IBAction func add_btn(_ sender: UIButton)
    {
        name = name_txt.text!
        
        //connects and opens db
        let fileURL = try! FileManager.default.url(for:.documentDirectory, in: .userDomainMask,appropriateFor: nil, create: false).appendingPathComponent("partyplanner.db")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK
        {
            print("error opening database")
        }
        
        //create your table if it doesnt already exist
        if sqlite3_exec(db, "create table if not exists partyList (id integer primary key autoincrement, name varchar)",nil,nil,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
        
        //create the statement, prepare the statement, bind values to statement, prep st, finalize st
        
        //insert
        let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, "insert into partyList (name) values(?)", -1, &statement, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error prepping statement: \(errmsg)")
        }
        
        ///bind as many times as you have variables going into table
        //important parts are statemnt, second integer, and name
        if sqlite3_bind_text(statement, 1, self.name , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error binding name: \(errmsg)")
        }
        
        
        //only one step
        if sqlite3_step(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error stepping: \(errmsg)")
        }
        
        
        //only have one finalize
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing: \(errmsg)")
        }
        
        //reset statement
        statement = nil
        
        
        //close and reset db
        if sqlite3_close(db) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error closing db : \(errmsg)")
        }

        db = nil
        
    }


    
}

