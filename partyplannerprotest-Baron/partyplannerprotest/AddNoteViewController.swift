//
//  AddNoteViewController.swift
//  partyplannerprotest
//
//  Created by Baron Heinrich on 10/28/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class AddNoteViewController: UIViewController {

    @IBOutlet weak var note_txt: UITextField!
    

    
    var name = ""
    var SelectedParty:String!

    override func viewDidLoad() {

        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addNote(_ sender: UIButton)
    {
     
        name = note_txt.text!
        
        //connects and opens db
        let fileURL = try! FileManager.default.url(for:.documentDirectory, in: .userDomainMask,appropriateFor: nil, create: false).appendingPathComponent("partyplanner.db")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK
        {
            print("error opening database")
        }
        
        //create your table if it doesnt already exist
        if sqlite3_exec(db, "create table if not exists notes (id integer primary key autoincrement, partyId integer, name varchar, FOREIGN KEY(partyId) REFERENCES partyList(id) )",nil,nil,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
        
        //create the statement, prepare the statement, bind values to statement, prep st, finalize st
        
        //insert
        let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, "insert into notes (name) values(?)", -1, &statement, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error prepping statement: \(errmsg)")
        }
        
        ///bind as many times as you have variables going into table
        //important parts are statemnt, second integer, and name
        if sqlite3_bind_text(statement, 1, self.name , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error binding name: \(errmsg)")
        }
        
        
        //only one step
        if sqlite3_step(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error stepping: \(errmsg)")
        }
        
        
        //only have one finalize
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing: \(errmsg)")
        }
        
        //reset statement
        statement = nil
        
        
        //close and reset db
        if sqlite3_close(db) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error closing db : \(errmsg)")
        }
        
        db = nil
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
