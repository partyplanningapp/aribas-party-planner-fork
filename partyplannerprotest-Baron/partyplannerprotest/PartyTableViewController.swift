//
//  PartyTableViewController.swift
//  partyplannerprotest
//
//  Created by Baron Heinrich on 10/13/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class PartyTableViewController: UITableViewController {

    
                ////////////////////////////////////////////
        /////////////TABLE ARRAYS///////////////////
        ////////////////////////////////////////////
        var partyNames = [String]()
        var partyList: [String: String] = [:];
        var theId = 0
   
        
        override func viewDidLoad() {
            //self.tableView.backgroundColor = UIColor.purple
            self.tableView.backgroundColor = UIColor(red: 122/255, green: 0/255, blue: 89/255, alpha: 1.0)
            super.viewDidLoad()
            
            
            //open Database
            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent("partyplanner.db")
            
            
            
            var db: OpaquePointer? = nil
            if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
                print("error opening database")
            }
            
            //create your table if it doesnt already exist
            if sqlite3_exec(db, "create table if not exists partyList (id integer primary key autoincrement, name varchar)",nil,nil,nil) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error creating table: \(errmsg)")
            }
            
            
            
            
            //query database
            _ = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
            var statement: OpaquePointer? = nil
            
            if sqlite3_prepare_v2(db, "select * from partyList", -1, &statement, nil) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error preparing select: \(errmsg)")
            }
            
            
            while sqlite3_step(statement) == SQLITE_ROW {
                let id = sqlite3_column_int64(statement, 0)
                print("id = \(id); ", terminator: "")
                
                if let name = sqlite3_column_text(statement, 1) {
                    let nameString = String(cString: name)
                    partyNames.append(nameString)
                    partyList[nameString]=String(id)
                    
                    print("party name = \(nameString)")
                } else {
                    print("name not found")
                }
                
                
            }
            
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing prepared statement: \(errmsg)")
            }
            
            statement = nil
            
            
            
            //close db & set to nil
            //if sqlite3_close(db) != SQLITE_OK {
            //  print("error closing database")
            //}
            
            // db = nil
            print(partyNames)
            print(partyList)
            
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        // MARK: - Table view data source
        
        
        /*
         override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
         // Configure the cell...
         return cell
         }
         */
        
        
        
        override func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            //var cell = UITableViewCell() as! AleTableViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = partyNames[indexPath.item]
            
            cell.backgroundColor = UIColor.clear
            cell.textLabel?.textColor = UIColor.white
            
            return cell
        }
        
        
        
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if section == 0
            {
                return partyNames.count
            }
            else
            {
                return 1//number of color accents
            }
    }
    
        
        // Override to support conditional editing of the table view.
        override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            // Return false if you do not want the specified item to be editable.
            return true
        }
        
        
        // Override to support editing the table view.
        override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {
                // Delete the row from the data source
                let name = partyNames[indexPath.row]
                partyNames.remove(at: indexPath.row)
                
                tableView.deleteRows(at: [indexPath], with: .fade)
                //remove from DB
                
                
                //open Database
                let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    .appendingPathComponent("partyplanner.db")
                
                
                
                var db: OpaquePointer? = nil
                if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
                    print("error opening database")
                }
                
                let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
                var statement: OpaquePointer? = nil
                
                if sqlite3_prepare_v2(db, "delete from partyList where name = (?)", -1, &statement, nil) != SQLITE_OK {
                    let errmsg = String(cString: sqlite3_errmsg(db))
                    print("error preparing select: \(errmsg)")
                }
                
                if sqlite3_bind_text(statement, 1, name, -1, SQLITE_TRANSIENT) != SQLITE_OK {
                    let errmsg = String(cString: sqlite3_errmsg(db))
                    print("failure binding name delete: \(errmsg)")
                }
                
                //one step
                if sqlite3_step(statement) != SQLITE_DONE {
                    let errmsg = String(cString: sqlite3_errmsg(db))
                    print("failure deleting party: \(errmsg)")
                }
                
                
                //finalize & reset statement
                if sqlite3_finalize(statement) != SQLITE_OK {
                    let errmsg = String(cString: sqlite3_errmsg(db))
                    print("error finalizing prepared statement: \(errmsg)")
                }
                
                statement = nil
                
                //close db & set to nil
                if sqlite3_close(db) != SQLITE_OK {
                    print("error closing database")
                }
                
                db = nil
                
                
            } else if editingStyle == .insert {
                // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
            }
        }
    
//    var valueToPass:String!
//    
//    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
//        print("You selected cell #\(indexPath.row)!")
//        
//        // Get Cell Label
//        let indexPath = tableView.indexPathForSelectedRow;
//        let currentCell = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
//        
//        valueToPass = currentCell?.textLabel?.text
//        performSegue(withIdentifier: "notes", sender: self)
//        
//    }
//    
//    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        
//        if (segue.identifier == "notes") {
//            
//            // initialize new view controller and cast it as your view controller
//              let destination = segue.destination as? NotesTableViewController
//            // your new view controller should have property that will store passed value
//            
//            let cell = sender as! UITableViewCell
//            let selectedRow = tableView.indexPath(for: cell)!.row
//            destination!.selectedValue = partyNames[selectedRow]
//            
//        }
//}
    
        /*
         // Override to support rearranging the table view.
         override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
         }
         */
        
        /*
         // Override to support conditional rearranging of the table view.
         override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
         // Return false if you do not want the item to be re-orderable.
         return true
         }
         */
        
        /*
          MARK: - Navigation
          In a storyboard-based application, you will often want to do a little preparation before navigation
     */
    
    
         override func prepare(for segue: UIStoryboardSegue, sender: Any?)
         {
//          Get the new view controller using segue.destinationViewController.
//          Pass the selected object to the new view controller.
            
//            if segue.identifier == "notes" ,
//                let nextScene = segue.destination as? NotesTableViewController ,
//                let indexPath = self.tableView.indexPathForSelectedRow {
//                let selectedVehicle = vehicles[indexPath.row]
//                nextScene.currentVehicle = selectedVehicle
           // }
            
            //query db for id of entry with partyName[selectedRow]
            
            if segue.identifier == "notes"

            {
                
                //var selectedId=""
                
                //let cell = sender as! UITableViewCell
                //let selectedRow = tableView.indexPath(for: cell)!.row
                
                //let myString = String(selectedRow)
                
                let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
                
                let currentCell = tableView.cellForRow(at: indexPath!)! as UITableViewCell
                
                print(currentCell.textLabel?.text)
                
                print(partyList[(currentCell.textLabel?.text)!])
                
                let selectedParty = partyList[(currentCell.textLabel?.text)!]
                
                let destination = segue.destination as? NotesTableViewController
                
                
//                let cell = sender as! UITableViewCell
//                let selectedRow = tableView.indexPath(for: cell)!.row
                
                destination?.SelectedValue = selectedParty
                
                
            }
         }
    
        
}
