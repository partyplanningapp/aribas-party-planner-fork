//
//  NoteDetailViewController.swift
//  party planner pro
//
//  Created by Ariba Siddiqui on 9/30/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class NoteDetailViewController: UIViewController {
    
    var note: Note!
    let defaults = UserDefaults.standard

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        titleTextField.text = defaults.object(forKey: "title") as! String?
        contentTextField.text = defaults.object(forKey: "content") as! String!
        
        //titleTextField.text = note.title
        //contentTextField.text = note.content
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        note.title = titleTextField.text!
        note.content = contentTextField.text
        
        defaults.set(note.title, forKey: "title")
        defaults.set(note.content, forKey: "content")


    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
